#!/bin/bash
# Meant to be called by install_android.sh

trap 'echo "AutoFlash process aborted."; exit 0' 1 2 3 8 9 14 15

echo "***********************************!!! WARNING !!!***********************************"
echo "After 15 seconds, the system will upload images through the launch script /opt/images/AutoFlash/install.sh"
echo "After the system performs the script, the power will be turned off."
echo If you want to stop this process, you must login as root and run CMD "kill $$"
echo "***********************************!!! WARNING !!!***********************************"

(sleep 15; echo Start download...)
(sleep 1; /opt/images/autoinstall.sh mmcblk0; )
(sleep 1; shutdown -h now;)

exit 0