# Copyright (C) 2019 O.S. Autogramma LLC.

DESCRIPTION = "Extra files for SDU work environment."
LICENSE = "LGPLv2"
LIC_FILES_CHKSUM = "file://LICENSE;md5=39ec502560ab2755c4555ee8416dfe42"

SRC_URI = "file://gpio.bin \
           file://gpio_def_init.bin \
           file://gpio_env_init.bin \
           file://gpio_env_test.bin \
           file://LICENSE"

S = "${WORKDIR}"

do_install () {
    install -d ${D}/${bindir}
    install -m 755 ${S}/gpio.bin ${D}/${bindir}/gpio
    install -m 755 ${S}/gpio_def_init.bin ${D}/${bindir}/gpio_def_init
    install -m 755 ${S}/gpio_env_init.bin ${D}/${bindir}/gpio_env_init
    install -m 755 ${S}/gpio_env_test.bin ${D}/${bindir}/gpio_env_test

}
